/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/08 13:12:38 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/04/03 10:22:44 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strlen(const char *s)
{
	int	len;

	len = 0;
	while (s[len] != '\0')
		++len;
	return (len);
}
/*
#include <stdio.h>
#include <string.h>
int main(void)
{
  char nb[] = "Hi Everybody";
  printf(" ft_strlen: %s ?  %i\n\n", nb, ft_strlen(nb));
  printf(" strlen: %s ?  %lu\n\n", nb, strlen(nb));
}
*/
