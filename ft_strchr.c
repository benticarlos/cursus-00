/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/09 14:19:49 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 09:27:25 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	while (*s && *s != (char)c)
		s++;
	if (*s == (char)c)
		return ((char *)s);
	return (NULL);
}
/*
#include <stdio.h>
#include <string.h>
int main(void)
{
  char *r;
  char *r2;
  char str[] = "Everybody";
  r = ft_strchr(str, 'y');
  printf("ft_strchr: y in %s.\n Result: %s\n\n", str, r);
  r2 = strchr(str, 'y');
  printf("strchr: y in %s.\n Result: %s\n\n", str, r2);
  return (0);
}
*/
