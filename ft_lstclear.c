/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/16 12:56:42 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/28 12:22:13 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **lst, void (*del)(void*))
{
	t_list	*tmp;

	while (*lst != NULL)
	{
		tmp = (*lst)->next;
		del((*lst)->content);
		free(*lst);
		*lst = tmp;
	}
	*lst = NULL;
}

/*
static void free_content(void *content)
{
	ft_bzero(content, sizeof(content));
}

#include <stdio.h>
int main(void)
{
    t_list *head = ft_lstnew("HEAD");
    ft_lstadd_front(&head, ft_lstnew("Node 1"));
    ft_lstadd_front(&head, ft_lstnew("Node 2"));
    ft_lstadd_front(&head, ft_lstnew("Node 3"));

    printf("Before ft_lstclear:\n");
    t_list *current_node = head;
    while (current_node != NULL)
    {
        printf(" %s ->", (char *)current_node->content);
        current_node = current_node->next;
    }
    printf("\n\n");
    
    printf("After ft_lstclear:\n");
    current_node = NULL;
    if (!current_node)
      printf(" Is Clear Nodes List...");
    //ft_lstclear(&head, free_content);
    while (current_node != NULL)
    {
        printf(" %s ->", (char *)current_node->content);
        current_node = current_node->next;
    }
    printf("\n\n");
    return 0;
}
*/
