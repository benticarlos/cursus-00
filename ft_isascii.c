/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isascii.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/08 12:34:13 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/25 15:15:22 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_isascii(int c)
{
	return (c >= 0 && c < 128);
}
/*
int main(void)
{
  printf(" ft_isascii: Tab?  %i\n", ft_isascii(9));
  printf(" ft_isascii: 2?  %i\n", ft_isascii(50));
  printf(" ft_isascii: A?  %i\n\n", ft_isascii(65));
  return 0;
}
*/
