/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isprint.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/08 12:47:51 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/16 18:31:01 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_isprint(int c)
{
	return (c >= 32 && c <= 126);
}
/*
int main(void)
{
	printf(" ft_isprint: Tab?  %i\n", ft_isprint(9));
	printf(" ft_isprint: 2?  %i\n", ft_isprint(50));
	printf(" ft_isprint: A?  %i\n\n", ft_isprint(65));
	return 0;
}
*/
