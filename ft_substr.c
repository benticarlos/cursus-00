/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/14 10:49:56 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/04/03 11:16:29 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	size_t	i;
	size_t	j;
	size_t	slen;
	char	*str;
	char	*str_s;

	if (s == NULL)
		return (NULL);
	slen = ft_strlen(s);
	if (slen <= start || len == 0)
		return (ft_strdup(""));
	i = ft_strlen(&s[start]);
	if (len > i)
		j = i + 1;
	else
		j = len + 1;
	str = (char *)malloc(j * sizeof(char));
	if (str == 0)
		return (0);
	str_s = (char *)s;
	ft_strlcpy(str, &str_s[start], j);
	return (str);
}
/*
#include <stdio.h>
int	main(void)
{
  char	str[] = "Everybody";

  char *r = ft_substr(str, 4, 5);
  printf("ft_substr: (origin) %s \n --> (result) %s\n\n", str, r);
  return (0);
}
*/
