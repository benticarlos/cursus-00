/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsize.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/16 10:28:59 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 16:52:49 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_lstsize(t_list *lst)
{
	size_t	size;

	size = 0;
	while (lst)
	{
		lst = lst->next;
		size++;
	}
	return (size);
}
/*
#include <stdio.h>
int main(void)
{
  t_list *head = ft_lstnew("Node 1");
  ft_lstadd_front(&head, ft_lstnew("Node 2"));
  ft_lstadd_front(&head, ft_lstnew("Node 3"));
  ft_lstadd_front(&head, ft_lstnew("Node 4"));

  int size = ft_lstsize(head);
  printf("ft_lstsize: (List size) %d\n", size);
  return (0);
}
*/
