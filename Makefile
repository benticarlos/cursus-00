# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/03/04 10:20:07 by cbenti-r          #+#    #+#              #
#    Updated: 2023/03/28 12:22:22 by cbenti-r         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft.a

AR = ar
ARFLAGS = crs

CC = gcc
CFLAG = -Wall -Wextra -Werror

RM = rm
RMFLAG = -f

INCLUDES = ./libft.h

SRC_PART1 = \
	./ft_isalpha.c 	\
	./ft_isdigit.c 	\
	./ft_isalnum.c 	\
	./ft_isascii.c 	\
	./ft_isprint.c 	\
	./ft_strlen.c 	\
	./ft_memset.c 	\
	./ft_bzero.c 	\
	./ft_memcpy.c 	\
	./ft_memmove.c 	\
	./ft_strlcpy.c 	\
	./ft_strlcat.c 	\
	./ft_toupper.c 	\
	./ft_tolower.c 	\
	./ft_strchr.c 	\
	./ft_strrchr.c 	\
	./ft_strncmp.c 	\
	./ft_memchr.c 	\
	./ft_memcmp.c 	\
	./ft_strnstr.c 	\
	./ft_atoi.c		\
	./ft_calloc.c 	\
	./ft_strdup.c

SRC_PART2 = \
	./ft_substr.c 		\
	./ft_strjoin.c 		\
	./ft_strtrim.c 		\
	./ft_split.c 		\
	./ft_itoa.c 		\
	./ft_strmapi.c 		\
	./ft_striteri.c		\
	./ft_putchar_fd.c	\
	./ft_putstr_fd.c	\
	./ft_putendl_fd.c	\
	./ft_putnbr_fd.c	

SRC = \
	$(SRC_PART1) $(SRC_PART2)

OBJS = $(SRC:.c=.o)

SRC_BONUS = \
		./ft_lstnew.c		\
		./ft_lstadd_front.c	\
		./ft_lstsize.c		\
		./ft_lstlast.c		\
		./ft_lstadd_back.c	\
		./ft_lstdelone.c	\
		./ft_lstclear.c		\
		./ft_lstiter.c		\
		./ft_lstmap.c		

OBJS_BONUS = $(SRC_BONUS:.c=.o)

all : $(NAME) 

clean :
	$(RM) $(RMFLAG) $(OBJS) $(OBJS_BONUS)

fclean : clean
	$(RM) $(RMFLAG) $(NAME) $(OBJS_BONUS)

re : fclean all

.c.o : $(SRC) $(INCLUDES)
	$(CC) $(CFLAG) -c $< -o $(<:.c=.o)

$(NAME) : $(OBJS)
	$(AR) $(ARFLAGS) $@ $^

bonus: $(OBJS) $(OBJS_BONUS)
	$(AR) $(ARFLAGS) $(NAME) $^

.PHONY : all clean fclean re bonus
