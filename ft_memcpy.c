/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/08 15:54:25 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/25 15:22:32 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	void	*origin_dest;

	if (!dest && !src)
		return (0);
	origin_dest = dest;
	while (n--)
		*((char *)dest++) = *((char *)src++);
	return (origin_dest);
}
/*
int main(void)
{
  char source[] = "Everybody";
  char destiny[] = "Hello";
  size_t num = sizeof(source);
  void *result;
  printf("Source ft_memcpy: %s\n", source);
  result = ft_memcpy(destiny, source, num);
  printf(" form ft_memcpy: %s\n\n", (char *)result);
}
*/
