/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/14 20:50:39 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 17:24:00 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*result;
	size_t	len;
	size_t	i;

	len = ft_strlen(s);
	result = (char *)malloc(sizeof(char) * (len + 1));
	if (!(result))
		return (0);
	i = 0;
	while (i < len)
	{
		result[i] = f(i, s[i]);
		i++;
	}
	result[len] = '\0';
	return (result);
}

/*
static char	mixer(unsigned int a, char b)
{
	return (b + a);
}

#include <stdio.h>
int main()
{
	char	digits[] = "aaaaaaaaaaaaAAAAAAAAAAAAA";
	printf("ft_strmapi: (origin) %s\n",digits);
	char	*new = ft_strmapi(digits, mixer);
	printf("ft_strmapi: (result) %s\n", new);
}
*/
