/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/11 15:04:31 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 10:01:26 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t	i;
	size_t	c;
	size_t	n_len;
	char	*hay;

	hay = (char *)haystack;
	n_len = ft_strlen(needle);
	if (n_len == 0 || haystack == needle)
		return (hay);
	i = 0;
	while (hay[i] != '\0' && i < len)
	{
		c = 0;
		while (hay[i + c] != '\0' && needle[c] != '\0' && \
				hay[i + c] == needle[c] && i + c < len)
			c++;
		if (c == n_len)
			return (hay + i);
		i++;
	}
	return (0);
}
/*
#include <stdio.h>
#include <string.h>
int main(void)
{
  char f1[] = "Everybody";
  char f2[] = "body";
  char *r1;
  char *r2;
  printf("String1 ft_strnstr: %s\n", f1);
  printf("String2 ft_strnstr: %s\n", f2);
  r1 = ft_strnstr(f1, f2, 12);
  printf(" ft_strnstr:  %s\n\n", r1);
  r2 = strnstr(f1, f2, 12);
  printf(" strnstr:  %s\n\n", r2);
  return (0);
}
*/
