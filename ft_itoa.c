/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/14 18:10:22 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/04/02 16:56:45 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_count_digit(int n)
{
	size_t	count;

	count = 0;
	while (n)
	{
		n /= 10;
		count++;
	}
	return (count);
}

static void	ft_write_nbrber(char *dest, unsigned int n)
{
	if (n < 10)
		*dest = n + '0';
	else
	{
		*dest = n % 10 + '0';
		ft_write_nbrber(dest - 1, n / 10);
	}
}

char	*ft_itoa(int n)
{
	unsigned int	nbr;
	char			*result;
	size_t			len;

	nbr = n;
	if (n == 0)
		return (ft_strdup("0"));
	else
	{
		if (n < 0)
			len = ft_count_digit(n) + 1;
		else
			len = ft_count_digit(n);
		result = (char *)malloc(sizeof(char) * (len + 1));
		if (!(result))
			return (0);
		if (n < 0)
			ft_write_nbrber((result + len - 1), -nbr);
		else
			ft_write_nbrber((result + len - 1), nbr);
		if (n < 0)
			*result = '-';
		result[len] = '\0';
	}
	return (result);
}
/*
#include <stdio.h>
int	main(void)
{
	//int	a = -456;
	int	a = -2147483648;
	printf("ft_itoa: (integer) %d ?\n result: (string) '%s'\n\n", a, ft_itoa(a));
	return (0);
}
*/
