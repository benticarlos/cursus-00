/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_back.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/16 11:08:30 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 16:58:11 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_back(t_list **lst, t_list *new)
{
	if (*lst == 0)
		*lst = new;
	else
		(ft_lstlast(*lst))->next = new;
}

/*
#include <stdio.h>
int main(void)
{
  t_list *head = ft_lstnew("Node 1");
  ft_lstadd_front(&head, ft_lstnew("Node 2"));
  ft_lstadd_front(&head, ft_lstnew("Node 3"));

  ft_lstadd_back(&head, ft_lstnew("Node 4"));

  t_list *current_node = head;
  printf("ft_lstadd_back: ");
  while (current_node != NULL)
  {
    printf("%s -> ", (char *)current_node->content);
    current_node = current_node->next;
  }
  printf("\n\n");
  return 0;
}
*/
