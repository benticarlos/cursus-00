/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstiter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/16 13:04:43 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/31 09:53:10 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstiter(t_list *lst, void (*f)(void *))
{
	while (lst)
	{
		(*f)(lst->content);
		lst = lst->next;
	}
}

/*
#include <stdio.h>
static void	print_content(void *content)
{
	printf("%s ", (char*)content);
}

int main(void)
{
	t_list *head = ft_lstnew("Node 1");
	ft_lstadd_front(&head, ft_lstnew("Node 2"));
	ft_lstadd_front(&head, ft_lstnew("Node 3"));
	ft_lstadd_front(&head, ft_lstnew("Node 4"));

	printf("ft_lstiter: ");
	ft_lstiter(head, print_content);
	printf("\n\n");

	return 0;
}
*/
