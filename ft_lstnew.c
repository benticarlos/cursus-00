/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/15 15:03:45 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 16:46:52 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void *content)
{
	t_list	*result;

	result = malloc(sizeof(t_list));
	if (!(result))
		return (0);
	result->content = content;
	result->next = 0;
	return (result);
}

/*
#include <stdio.h>
int	main(void)
{ 	
  char *str = "New List Node";
  t_list *new_node = ft_lstnew(str);
  printf("ft_lstnew: (Content) %s\n", (char *)(new_node->content));
  printf("ft_lstnew: (Memory) %p\n\n", (char *)(new_node->content));
  return 0;
}
*/
