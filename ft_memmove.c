/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/09 09:16:27 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 11:51:37 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	unsigned char		*dp;
	const unsigned char	*sp;

	if (!dest && !src)
		return (0);
	if (dest > src)
	{
		dp = dest + n;
		sp = src + n;
		while (n-- > 0)
			*--dp = *--sp;
	}
	else
	{
		dp = dest;
		sp = src;
		while (n-- > 0)
			*dp++ = *sp++;
	}
	return (dest);
}
/*
#include <stdio.h>
#include <string.h>
int main(void)
{

  char source[] = "Everybody";
  char destiny[] = "Hello";

  printf("Source: %s\n", source);
  printf("Destiny: %s\n", destiny);
  void *result = ft_memmove(destiny, source, sizeof(source));
  printf(" from ft_memmove: %s\n", (char *)result);
  printf("Result Destiny: %s\n\n", destiny);
  
  char source2[] = "Everybody";
  char destiny2[] = "         ";
  printf("memmove Source: %s\n", source2);
  printf("memmove Destiny: %s\n", destiny2);
  void *result2 = memmove(destiny2, source2, sizeof(source2));
  printf(" from memmove: %s\n\n", (char *)result2);
  return (0);
}
*/
