/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/11 14:21:31 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 14:58:34 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	const unsigned char	*conv1;
	const unsigned char	*conv2;
	size_t				i;

	i = 0;
	conv1 = (const unsigned char *)s1;
	conv2 = (const unsigned char *)s2;
	while (i < n)
	{
		if (conv1[i] != conv2[i])
			return (conv1[i] - conv2[i]);
		i++;
	}
	return (0);
}

/*
#include <stdio.h>
#include <string.h>
int main(void)
{
  char p1[] = "Fverybody";
  char p2[] = "Everybody";
  printf("String1 ft_memcmp: %s\n", p1);
  printf("String2 ft_memcmp: %s\n", p2);
  int r = ft_memcmp(p1, p2, 9);
  printf(" ft_memcmp:  %i\n\n", r);
  int r2 = memcmp(p1, p2, 9);
  printf(" memcmp:  %i\n\n", r2);
  return (0);
}
*/
