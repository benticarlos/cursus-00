/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/08 14:37:59 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 15:12:16 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_bzero(void *s, size_t n)
{
	ft_memset(s, '\0', n);
}

/*
#include <stdio.h>
#include <string.h>
int main(void)
{
  char str[] = "Everybody";
  char str1[] = "Hello Everybody";
  printf("Origin ft_bzero: %s\n", str);
  ft_bzero(str, 5);
  printf("Modified ft_bzero: %s\n\n", str);
  printf("Origin bzero: %s\n", str1);
  bzero(str1, 8);
  printf("Modified bzero: %s\n\n", str1);
  return (0);
}
*/
