/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/13 20:18:38 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 09:12:38 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcpy(char *dest, char *src, size_t size)
{
	size_t	index;

	index = 0;
	if (size > 0)
	{
		while (index < (size - 1) && src[index])
		{
			dest[index] = src[index];
			index++;
		}
		dest[index] = '\0';
	}
	while (src[index])
		index++;
	return (index);
}
/*
#include <stdio.h>
#include <string.h>
int main(void)
{
  char src[] = "Hello";
  char dst[] = " Everybody";
  char dst1[] = " Todos";
  printf("ft_strlcpy: (Old src) %s\n", src);
  printf("ft_strlcpy: (Old dst) %s\n", dst);
  printf("ft_strlcpy: (Old dst1) %s\n", dst1);
  printf(" ft_strlcpy:  %zu\n", ft_strlcpy(dst, src, 15));
  printf("ft_strlcpy: (New src) %s\n", src);
  printf("ft_strlcpy: (New dst) %s\n\n", dst);
  printf(" strlcpy:  %zu\n", strlcpy(dst1, src, 7));
  printf("strlcpy: (New src) %s\n", src);
  printf("strlcpy: (New dst1) %s\n\n", dst1);
}
*/
