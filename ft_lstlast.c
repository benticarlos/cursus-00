/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstlast.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/16 10:49:01 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 16:55:56 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstlast(t_list *lst)
{
	if (!lst)
		return (0);
	while (lst->next)
		lst = lst->next;
	return (lst);
}

/*
#include <stdio.h>
int main(void)
{
  t_list *head = ft_lstnew("Node 1");
  ft_lstadd_front(&head, ft_lstnew("Node 2"));
  ft_lstadd_front(&head, ft_lstnew("Node 3"));
  ft_lstadd_front(&head, ft_lstnew("Node 4"));

  t_list *last = ft_lstlast(head);
  printf("ft_lstlast: (Last Node) %s\n", (char*)last->content);
  return (0);
}
*/
