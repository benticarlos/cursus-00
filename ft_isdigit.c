/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isdigit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/04 11:47:06 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/28 12:03:41 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_isdigit(int c)
{
	return (c >= '0' && c <= '9');
}
/*
int main(void)
{
  printf(" ft_isdigit: 2?  %i\n", ft_isdigit(50));
  printf(" ft_isdigit: A?  %i\n\n", ft_isdigit(65));
}
*/
