/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/10 09:34:46 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 09:36:30 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	*start;

	start = (char *)s;
	while (*s)
		s++;
	while (s >= start)
	{
		if (*s == (char)c)
			return ((char *)s);
		s--;
	}
	return (0);
}
/*
#include <stdio.h>
#include <string.h>
int main(void)
{
  char *r;
  char *r2;
  char str[] = "Everybody";
  char str2[] = "Everybody";
  r = ft_strrchr(str, 'e');
  printf(" ft_strchr: e in %s.\n Result: %s\n\n", str, r);
  r2 = strrchr(str, 'e');
  printf(" strchr: e in %s.\n Result: %s\n\n", str2, r2);
  return 0;
}
*/
