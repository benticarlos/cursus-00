/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/14 21:13:25 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 16:41:03 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_striteri(char *s, void (*f)(unsigned int, char*))
{
	size_t	index;

	index = 0;
	if (s)
	{
		while (s[index])
		{
			f(index, &s[index]);
			index++;
		}
	}
}
/*
void	mixer(unsigned int a, char *b)
{
	*b = *b+a;
}

#include <stdio.h>
int main()
{
	char digits[] = "aaaaaaa";
	printf("ft_striteri: (origin) %s\n",digits);
	ft_striteri(digits, mixer);
	printf("ft_striteri: (result) %s\n\n",digits);
}
*/
