/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/16 13:37:53 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/04/02 12:26:08 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static t_list	*ft_lst_new(void *content)
{
	t_list	*result;

	result = ft_calloc(sizeof(t_list), sizeof(t_list));
	if (!(result))
		return (0);
	result->content = content;
	result->next = 0;
	return (result);
}

t_list	*ft_lstmap(t_list *lst, void *(*f)(void*), void (*del)(void *))
{
	t_list	*new;
	t_list	*new_lst;
	t_list	*tmp;

	if (!lst || !f)
		return (NULL);
	new = ft_lst_new(f(lst->content));
	if (!new)
		return (NULL);
	new_lst = new;
	lst = lst->next;
	while (lst)
	{
		tmp = ft_lst_new(f(lst->content));
		if (!tmp)
		{
			ft_lstclear(&new_lst, del);
			return (NULL);
		}
		new->next = tmp;
		new = new->next;
		lst = lst->next;
	}
	return (new_lst);
}

/*
#include <stdio.h>
static void	*add_one(void *content)
{
	t_list *new_node;

    new_node = malloc(sizeof(t_list));
	new_node->content = content;
	new_node->next = 0;
    return (new_node);
}

static void	free_content(void *content)
{
    ft_bzero(content, sizeof(content));
}

int	main(void)
{
	t_list	*head = ft_lst_new("HEAD");
	t_list	*new_list;
	t_list	*current_node;

    ft_lstadd_front(&head, ft_lst_new("Node 1"));
    ft_lstadd_front(&head, ft_lst_new("Node 2"));
    ft_lstadd_front(&head, ft_lst_new("Node 3"));

    printf("ft_lstmap: (Original List):\n");
    current_node = head;
    while (current_node != NULL)
    {
        printf("%s -> ", (char *)current_node->content);
        current_node = current_node->next;
    }
    printf("\n\n");
    
    printf("ft_lstmap: (New List):\n");
    new_list = ft_lstmap(head, &add_one, free_content);
    current_node = new_list;
    while (current_node != NULL)
    {
        printf("%s -> ", (char *)current_node->content);
        current_node = current_node->next;
    }
    printf("\n\n");
    //ft_lstclear(&head, free_content);
    //ft_lstclear(&new_list, free_content);
    return (0);
}
*/
