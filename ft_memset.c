/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/08 14:06:44 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 11:11:12 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char	*str;
	size_t			i;

	str = (unsigned char *)b;
	i = 0;
	while (i < len)
	{
		str[i] = (unsigned char)c;
		++i;
	}
	return (b);
}
/*
#include <stdio.h>
#include <string.h>
int main(void)
{
  char str[] = "Hello Everybody";
  char str2[] = "Hello Everybody";
  printf("Origin ft_memset: %s\n", str);
  ft_memset(str, '-', 5);
  printf("Modified ft_memset: %s\n\n", str);
  printf("Origin memset: %s\n", str2);
  memset(str2, '-', 5);
  printf("Modified memset: %s\n\n", str2);
  return (0);
}
*/
