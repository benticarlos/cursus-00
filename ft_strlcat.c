/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/09 11:40:02 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 10:57:38 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	char		*d;
	const char	*s;
	size_t		n;
	size_t		dlen;

	n = dstsize;
	d = dst;
	s = src;
	while (n-- != 0 && *d != '\0')
		++d;
	dlen = d - dst;
	n = dstsize - dlen;
	if (n == 0)
		return (dlen + ft_strlen(s));
	while (*s != '\0')
	{
		if (n != 1)
		{
			*d++ = *s;
			--n;
		}
		++s;
	}
	*d = '\0';
	return (dlen + (s - src));
}

/*
#include <stdio.h>
#include <string.h>

int	main(void)
{
	char	src[];
	char	dst[];

  src[] = "Hello";
  dst[] = " Everybody";
  printf("Source ft_strlcat: %s\n", src);
  printf("Destiny ft_strlcat: %s\n", dst);
  printf(" ft_strlcat:  %zu\n", ft_strlcat(dst, src, 11));
  printf(" ft_strlcat: (src)  %s\n", src);
  printf(" ft_strlcat: (dst)  %s\n\n", dst);
  printf(" strlcat:  %zu\n", strlcat(dst, src, 11));
  printf(" strlcat: (src)  %s\n", src);
  printf(" strlcat: (dst)  %s\n\n", dst);
}
*/
