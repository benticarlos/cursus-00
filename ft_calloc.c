/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/13 11:32:11 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/28 11:37:17 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_calloc(size_t nmemb, size_t size)
{
	void	*mem;

	if (nmemb >= SIZE_MAX)
		return (NULL);
	mem = (void *)malloc(nmemb * size);
	if (!mem)
		return (NULL);
	ft_bzero(mem, nmemb * size);
	return (mem);
}

/*
#include <stdio.h>
#include <string.h>
int	main(void)
{
	size_t	num_elem;
	size_t	size_elem;

    num_elem = 10;
    size_elem = sizeof(int);
    int *ptr1 = (int*)ft_calloc(num_elem, size_elem);
    int *ptr2 = (int*)ft_calloc(num_elem, size_elem);
    printf("ft_calloc: (ptr1): %p\n", ptr1);
    printf("ft_calloc: (ptr2): %p\n", ptr2);
    
    int *ptr3 = (int*)calloc(num_elem, size_elem);
    int *ptr4 = (int*)calloc(num_elem, size_elem);
    printf("ft_calloc: (ptr3): %p\n", ptr3);
    printf("ft_calloc: (ptr4): %p\n", ptr4);
    return (0);
}
*/
