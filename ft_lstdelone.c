/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/16 11:33:38 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/31 09:25:35 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdelone(t_list *lst, void (*del)(void *))
{
	if (lst && del)
	{	
		(*del)(lst->content);
		free(lst);
	}
}
/*
#include <stdio.h>
static void free_content(void *content)
{
	//free(content);
	ft_bzero(content, sizeof(content));
}

static void	print_content(void *content)
{
	printf(" %s ", (char*)content);
}

int	main()
{
	t_list *head = ft_lstnew("HEAD");
	t_list *node0 = ft_lstnew("node 0");
	t_list *node1 = ft_lstnew("node 1");
	t_list *node2 = ft_lstnew("node 2");

	ft_lstadd_back(&head, node0);
	ft_lstadd_back(&head, node1);
	ft_lstadd_back(&head, node2);
	
	ft_lstiter(head, print_content);
	printf("\n");
	ft_lstdelone(&node1, free_content);
	ft_lstiter(head, print_content);
	return 0;
}
*/
