/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/13 12:42:52 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 15:29:44 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s)
{
	char	*ret;
	size_t	index;
	size_t	len;

	len = ft_strlen(s) + 1;
	ret = malloc(sizeof(char) * len);
	if (!ret)
		return (NULL);
	index = 0;
	while (index < len)
	{
		ret[index] = s[index];
		index++;
	}
	return (ret);
}
/*
#include <stdio.h>
#include <string.h>
int	main(void)
{
  char f1[] = "Everybody";
  char f2[] = "Everybody";
  printf(" ft_strdup: (origin) --> %s \n \
	ft_strdup: (memory) --> %s\n\n", f1, ft_strdup(f1));
  printf(" strdup: (origin) --> %s \n \
	strdup: (memory) --> %s\n\n", f2, strdup(f2));
  return (0);
}
*/
