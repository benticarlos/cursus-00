/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/10 15:55:24 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 09:41:37 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	unsigned char	conv1;
	unsigned char	conv2;

	while (n > 0)
	{
		conv1 = *(unsigned char *)s1++;
		conv2 = *(unsigned char *)s2++;
		if (conv1 != conv2)
			return (conv1 - conv2);
		if (conv1 == '\0')
			return (0);
		n--;
	}
	return (0);
}
/*
#include <stdio.h>
#include <string.h>
int main(void)
{
  char str1[] = "Fverybody";
  char str2[] = "Everybody";
  int r1;
  int r2;
  printf("String1 ft_strncmp: %s\n", str1);
  printf("String2 ft_strncmp: %s\n", str2);
  r1 = ft_strncmp(str1, str2, 9);
  printf(" ft_strncmp:  %i\n\n", r1);
  r2 = strncmp(str1, str2, 9);
  printf(" strncmp:  %i\n\n", r2);
  return (0);
}
*/
