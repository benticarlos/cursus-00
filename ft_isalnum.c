/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalnum.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/08 12:18:30 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/25 15:13:51 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_isalnum(int c)
{
	return (ft_isdigit(c) || ft_isalpha(c));
}
/*
int main(void)
{
  printf(" ft_isalnum: Tab?  %i\n", ft_isalnum(9));
  printf(" ft_isalnum: A?  %i\n", ft_isalnum(65));
  printf(" ft_isalnum: 3?  %i\n\n", ft_isalnum(51));
}
*/
