/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/10 17:44:21 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/03/27 14:49:52 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	const unsigned char	*p;

	p = (const unsigned char *)s;
	while (n > 0)
	{
		if (*p == (unsigned char)c)
			return ((void *)p);
		p++;
		n--;
	}
	return (0);
}

/*
#include <stdio.h>
#include <string.h>
int main(void)
{
  char p1[] = "Everybody";
  char p2[] = "Everybody";
  char c = 'y';

  printf("String1 ft_memchr: %s\n", p1);
  char *r = ft_memchr(p1, c, 9);
  printf(" ft_memchr: find r?  %s\n\n", r);
  printf("String memchr: %s\n", p1);
  char *r2 = memchr(p2, c, 9);
  printf(" memchr: find r?  %s\n\n", r2);
  return (0);
}
*/
